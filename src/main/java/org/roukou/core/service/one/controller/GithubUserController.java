package org.roukou.core.service.one.controller;

import lombok.extern.slf4j.Slf4j;
import org.roukou.core.service.one.clients.github.GitHubLookupService;
import org.roukou.core.service.one.clients.github.impl.GitHubLookupServiceImpl;
import org.roukou.core.service.one.exceptions.GithubUserNotFoundException;
import org.roukou.core.service.one.model.GithubUser;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(value = "/core/user/v1.0")
public class GithubUserController {

    private final GitHubLookupService gitHubLookupService;

    public GithubUserController(GitHubLookupServiceImpl gitHubLookupService) {
        this.gitHubLookupService = gitHubLookupService;
    }

    @RequestMapping(value = "/user={name}", method = RequestMethod.GET)
    public GithubUser retrieveGithubUserByName(@PathVariable String name) {
        log.info("Retrieve github user by name={}", name);

        return gitHubLookupService.findGithubUser(name);
    }

    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public String handleNotFoundException( GithubUserNotFoundException ex)
    {
        return ex.getMessage();
    }
}
