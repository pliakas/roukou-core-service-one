package org.roukou.core.service.one.repository;

import java.util.List;
import java.util.Optional;
import org.roukou.core.service.one.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    Optional<Product> findByName(String name);
    Optional<List<Product>> findByServiceAddress(String serviceAddress);
}
