package org.roukou.core.service.one.exceptions;

public class GithubUserNotFoundException extends RuntimeException {

    public GithubUserNotFoundException(String message) {
        super(message);
    }
}
