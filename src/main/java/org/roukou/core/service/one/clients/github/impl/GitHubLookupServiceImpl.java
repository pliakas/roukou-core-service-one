package org.roukou.core.service.one.clients.github.impl;

import lombok.extern.log4j.Log4j2;
import org.roukou.core.service.one.clients.github.GitHubLookupService;
import org.roukou.core.service.one.model.GithubUser;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Log4j2
@Service
public class GitHubLookupServiceImpl implements GitHubLookupService {

    private final RestTemplate restTemplate;

    public GitHubLookupServiceImpl(RestTemplateBuilder builder) {
        this.restTemplate = builder.build();
    }

    @Override
    public GithubUser findGithubUser(String user) {
        log.info("find github user with name={}", user);
        String url = String.format("https://api.github.com/users/%s", user);
        return restTemplate.getForObject(url, GithubUser.class);
    }
}
