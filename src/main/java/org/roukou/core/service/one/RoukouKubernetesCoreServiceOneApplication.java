package org.roukou.core.service.one;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoukouKubernetesCoreServiceOneApplication {

    public static void main(String[] args) {
        SpringApplication.run(RoukouKubernetesCoreServiceOneApplication.class, args);
    }
}
