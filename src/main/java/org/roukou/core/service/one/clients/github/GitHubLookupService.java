package org.roukou.core.service.one.clients.github;

import org.roukou.core.service.one.model.GithubUser;

public interface GitHubLookupService {
    GithubUser findGithubUser(String user);
}
