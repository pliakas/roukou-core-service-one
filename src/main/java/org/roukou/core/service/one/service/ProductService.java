package org.roukou.core.service.one.service;

import org.roukou.core.service.one.model.Product;

public interface ProductService {
    Product getProductByName(String name);
}
