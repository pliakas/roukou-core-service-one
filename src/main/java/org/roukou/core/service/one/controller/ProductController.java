package org.roukou.core.service.one.controller;

import lombok.extern.slf4j.Slf4j;
import org.roukou.core.service.one.exceptions.ProductNotFoundException;
import org.roukou.core.service.one.model.Product;
import org.roukou.core.service.one.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(value = "/core/product/v1.0")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping(value = "/name={name}", method = RequestMethod.GET)
    public Product retrieveProductByName(@PathVariable String name) {
        log.info("Retrieve product by name={}", name);


        return productService.getProductByName(name);
    }

    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public String handleProductNotFoundException( ProductNotFoundException ex )
    {
        return ex.getMessage();
    }
}
