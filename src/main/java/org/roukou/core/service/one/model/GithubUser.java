package org.roukou.core.service.one.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.HashMap;
import java.util.Map;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class GithubUser {

    private String name;
    private String blog;

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

}
