package org.roukou.core.service.one.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.roukou.core.service.one.exceptions.ProductNotFoundException;
import org.roukou.core.service.one.model.Product;
import org.roukou.core.service.one.repository.ProductRepository;
import org.roukou.core.service.one.service.ProductService;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    @Override
    public Product getProductByName(String name) {
        log.info("Get product by name={}", name);



        return productRepository.findByName(name).orElseThrow(
            () -> new ProductNotFoundException("Product not found for name=" + name)
        );
    }
}
