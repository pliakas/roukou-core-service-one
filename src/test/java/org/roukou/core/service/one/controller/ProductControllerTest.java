package org.roukou.core.service.one.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.roukou.core.service.one.common.builders.GenericBuilder;
import org.roukou.core.service.one.exceptions.ProductNotFoundException;
import org.roukou.core.service.one.model.Product;
import org.roukou.core.service.one.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ProductController.class)
class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @Test
    void retrieveProductByName_Success() throws Exception {
        //arrange
        when( productService.getProductByName("product-name")).thenReturn(
                GenericBuilder.of(Product::new)
                    .with(Product::setId, 10L)
                    .with(Product::setName, "product-name" )
                    .with(Product::setServiceAddress, "product-service-address")
                    .build());

        //act && assert
        mockMvc.perform( MockMvcRequestBuilders.get( "/core/product/v1.0/name=product-name")
                .contentType( MediaType.APPLICATION_JSON )
                .accept( MediaType.APPLICATION_JSON ) )
            .andExpect( status().isOk() )
            .andExpect( jsonPath( "$.id", Matchers.is( 10) ) )
            .andExpect( jsonPath( "$.name", Matchers.is( "product-name") ) )
            .andExpect( jsonPath( "$.service_address", Matchers.is( "product-service-address") ) );
    }

    @Test
    void retrieveProductByName_Failure() throws Exception {
        //arrange
        when( productService.getProductByName("product-name")).thenThrow(ProductNotFoundException.class);

        //act && assert
        mockMvc.perform( MockMvcRequestBuilders.get( "/core/product/v1.0/name=product-name")
            .contentType( MediaType.APPLICATION_JSON )
            .accept( MediaType.APPLICATION_JSON ) )
            .andExpect( status().isNotFound() );
    }
}
