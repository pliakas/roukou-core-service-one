package org.roukou.core.service.one.model;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.testcontainers.shaded.com.fasterxml.jackson.core.JsonProcessingException;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

@JsonTest
class ProductTest {

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void skata() throws IOException {
        Product product = new Product();
        product.setName("mitsos");
        product.setServiceAddress("skata");

        System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(product));

        String input = "{\n"
            + "  \"id\" : null,\n"
            + "  \"name\" : \"fragkos\",\n"
            + "  \"paparas\" : \"pliaksa\",\n"
            + "  \"serviceAddress\" : \"skata\"\n"
            + "}\n";

        Product result = objectMapper.readValue(input, Product.class);
        assertEquals("fragkos", result.getName());


    }
}
