package org.roukou.core.service.one.common.builders;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class GenericBuilderTest {
    @Test
    public void of() throws Exception {
        String result = GenericBuilder.of(String::new).build();
        assertTrue(result instanceof String);
    }

    @Test
    public void with() throws Exception {
        SampleObject sampleObject = GenericBuilder.of(SampleObject::new)
            .with(SampleObject::setFirstEntry, "first")
            .with(SampleObject::setSecondEntry, "second")
            .build();

        assertTrue(sampleObject instanceof SampleObject);
        assertEquals("first", sampleObject.getFirstEntry());
        assertEquals("second", sampleObject.getSecondEntry());
    }

    class SampleObject {

        private String firstEntry;
        private String secondEntry;

        String getFirstEntry() {
            return firstEntry;
        }

        void setFirstEntry(String firstEntry) {
            this.firstEntry = firstEntry;
        }

        String getSecondEntry() {
            return secondEntry;
        }

        void setSecondEntry(String secondEntry) {
            this.secondEntry = secondEntry;
        }
    }

}
