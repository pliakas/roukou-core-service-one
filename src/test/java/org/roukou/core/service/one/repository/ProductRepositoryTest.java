package org.roukou.core.service.one.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.roukou.core.service.one.common.builders.GenericBuilder;
import org.roukou.core.service.one.model.Product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonComponent;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@DataJpaTest
@Testcontainers
@ExtendWith(SpringExtension.class)
@AutoConfigureTestDatabase( replace = Replace.NONE )
@ContextConfiguration(initializers = { ProductRepositoryTest.Initializer.class })
class ProductRepositoryTest {

    @Container
    private static final MariaDBContainer MARIA_DB_CONTAINER = new MariaDBContainer();

    @Autowired
    private ProductRepository productRepository;


    static class Initializer implements
        ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                "spring.datasource.url=" + MARIA_DB_CONTAINER.getJdbcUrl(),
                "spring.datasource.username=" + MARIA_DB_CONTAINER.getUsername(),
                "spring.datasource.password=" + MARIA_DB_CONTAINER.getPassword())
                .applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    @Test
    @DisplayName("Find product by name")
    void findProductByName() {

        //arrange
        AtomicInteger counter = new AtomicInteger(0);
        Stream.generate(
            () -> GenericBuilder.of(Product::new)
                .with(Product::setName, "test-product-name-" + counter.incrementAndGet())
                .with(Product::setServiceAddress, "test-service-address-" + counter.intValue())
                .build() ).limit(10).forEach(
                entry -> productRepository.save(entry)
        );

        //act
        var result = productRepository.findByName("test-product-name-2");

        //assert
        assertAll("Product found for name={test-product-name-2}",
            () -> assertTrue(result.isPresent()),
            () -> assertEquals("test-product-name-2", result.get().getName()),
            () -> assertEquals("test-service-address-2", result.get().getServiceAddress())
        );
    }
}
