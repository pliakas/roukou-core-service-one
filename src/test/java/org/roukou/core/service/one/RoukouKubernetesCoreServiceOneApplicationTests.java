package org.roukou.core.service.one;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.roukou.core.service.one.common.builders.GenericBuilder;
import org.roukou.core.service.one.model.Product;
import org.roukou.core.service.one.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {RoukouKubernetesCoreServiceOneApplicationTests.Initializer.class})
class RoukouKubernetesCoreServiceOneApplicationTests {
    @Container
    private static final MariaDBContainer MARIA_DB_CONTAINER = new MariaDBContainer();

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private ProductRepository repository;

    @BeforeEach
    void setUp() {
        AtomicInteger counter = new AtomicInteger(0);
        Stream.generate(
            () -> GenericBuilder.of(Product::new)
                .with(Product::setName, "test-product-name-" + counter.incrementAndGet())
                .with(Product::setServiceAddress, "test-service-address-" + counter.intValue())
                .build() ).limit(10).forEach(
            entry -> repository.save(entry)
        );
    }

    @AfterEach
    void tearDown() {
        repository.findAll().forEach(
            entry -> repository.delete(entry)
        );
    }

    @Test
    void retrieveProductByName_Success() {
        //act
        ResponseEntity<Product> created =
            testRestTemplate.getForEntity("/core/product/v1.0/name=test-product-name-5", Product.class);

        assertAll(
            () -> assertEquals(HttpStatus.OK, created.getStatusCode() ),
            () -> assertTrue(Objects.nonNull(created.getBody())),
            () -> assertEquals("test-product-name-5", created.getBody().getName()),
            () -> assertEquals("test-service-address-5", created.getBody().getServiceAddress() )
        );
    }

    @Test
    @Disabled
    void retrieveProductByName_Failure() {
        //act
        ResponseEntity<Product> created =
            testRestTemplate.getForEntity("/core/product/v1.0/name=test-product-name-18", Product.class);

        assertAll(
            () -> assertEquals(HttpStatus.NOT_FOUND, created.getStatusCode() ),
            () -> assertTrue(Objects.isNull(created.getBody()))
        );
    }


    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                "spring.datasource.url=" + MARIA_DB_CONTAINER.getJdbcUrl(),
                "spring.datasource.username=" + MARIA_DB_CONTAINER.getUsername(),
                "spring.datasource.password=" + MARIA_DB_CONTAINER.getPassword())
                .applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}
