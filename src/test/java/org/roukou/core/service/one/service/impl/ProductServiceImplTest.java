package org.roukou.core.service.one.service.impl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.roukou.core.service.one.common.builders.GenericBuilder;
import org.roukou.core.service.one.exceptions.ProductNotFoundException;
import org.roukou.core.service.one.model.Product;
import org.roukou.core.service.one.repository.ProductRepository;
import org.roukou.core.service.one.service.ProductService;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class ProductServiceImplTest {

    @MockBean
    private ProductRepository productRepository;

    private ProductService productService;

    @BeforeEach
    void setUp() {
        productService = new ProductServiceImpl(productRepository);
    }

    @Test
    void getProductByName_Success() {
        //assert
        when( productRepository.findByName("name")).thenReturn(
            Optional.ofNullable(
                GenericBuilder.of(Product::new)
                    .with(Product::setId, 2L)
                    .with(Product::setName, "test-product-name" )
                    .with(Product::setServiceAddress, "test-service-address")
                    .build() ) );

        //act
        var result = productService.getProductByName("name");

        //assert
        //arrange
        assertAll("Product found for name={name}",
            () -> assertEquals(2L, result.getId().longValue()),
            () -> assertEquals("test-product-name", result.getName()),
            () -> assertEquals("test-service-address", result.getServiceAddress())
        );
    }

    @Test
    void getProductByName_Faulure() {
        when(productRepository.findByName("name")).thenReturn(Optional.empty());

        assertThrows( ProductNotFoundException.class, () -> productService.getProductByName("name"));
    }
}
