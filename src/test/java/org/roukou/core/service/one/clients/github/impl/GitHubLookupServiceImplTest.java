package org.roukou.core.service.one.clients.github.impl;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.roukou.core.service.one.clients.github.GitHubLookupService;
import org.roukou.core.service.one.model.GithubUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.client.MockRestServiceServer;

@ExtendWith(SpringExtension.class)
@RestClientTest( GitHubLookupService.class)
class GitHubLookupServiceImplTest {

    @Autowired
    private GitHubLookupService gitHubLookupService;

    @Autowired
    private MockRestServiceServer mockRestServiceServer;

    @Autowired
    private ObjectMapper mapper;

    @Test
    void findGithubUser() {
        //arrage
        String expectedAnswer = "{\n"
            + "\"login\": \"pliakas\",\n"
            + "\"id\": 861938,\n"
            + "\"node_id\": \"MDQ6VXNlcjg2MTkzOA==\",\n"
            + "\"avatar_url\": \"https://avatars3.githubusercontent.com/u/861938?v=4\",\n"
            + "\"gravatar_id\": \"\",\n"
            + "\"url\": \"https://api.github.com/users/pliakas\",\n"
            + "\"html_url\": \"https://github.com/pliakas\",\n"
            + "\"followers_url\": \"https://api.github.com/users/pliakas/followers\",\n"
            + "\"following_url\": \"https://api.github.com/users/pliakas/following{/other_user}\",\n"
            + "\"gists_url\": \"https://api.github.com/users/pliakas/gists{/gist_id}\",\n"
            + "\"starred_url\": \"https://api.github.com/users/pliakas/starred{/owner}{/repo}\",\n"
            + "\"subscriptions_url\": \"https://api.github.com/users/pliakas/subscriptions\",\n"
            + "\"organizations_url\": \"https://api.github.com/users/pliakas/orgs\",\n"
            + "\"repos_url\": \"https://api.github.com/users/pliakas/repos\",\n"
            + "\"events_url\": \"https://api.github.com/users/pliakas/events{/privacy}\",\n"
            + "\"received_events_url\": \"https://api.github.com/users/pliakas/received_events\",\n"
            + "\"type\": \"User\",\n"
            + "\"site_admin\": false,\n"
            + "\"name\": \"Thomas Pliakas\",\n"
            + "\"company\": \"Roukou\",\n"
            + "\"blog\": \"http://roukou.org\",\n"
            + "\"location\": \"Athens, Greece\",\n"
            + "\"email\": null,\n"
            + "\"hireable\": true,\n"
            + "\"bio\": null,\n"
            + "\"public_repos\": 23,\n"
            + "\"public_gists\": 0,\n"
            + "\"followers\": 1,\n"
            + "\"following\": 4,\n"
            + "\"created_at\": \"2011-06-20T15:06:30Z\",\n"
            + "\"updated_at\": \"2019-09-21T07:13:17Z\"\n"
            + "}";

        this.mockRestServiceServer
            .expect( requestTo( "https://api.github.com/users/pliakas"))
            .andRespond(withSuccess( expectedAnswer, MediaType.APPLICATION_JSON) );

        //act
        GithubUser result = gitHubLookupService.findGithubUser("pliakas");

        //assert
        assertEquals("Thomas Pliakas", result.getName());
        assertEquals("http://roukou.org", result.getBlog());

    }
}
